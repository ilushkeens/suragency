'use strict'

// Функция для получения Cookies
function getCookie(name) {
  let matches = document.cookie.match(new RegExp('(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'));

  return matches ? decodeURIComponent(matches[1]) : undefined;
}

// Функция для установки заданной темы/цветовой схемы
function setTheme(themeName) {
  localStorage.setItem('theme', themeName);
  document.documentElement.className = themeName;

  // Смена цвета для метатегов и стилей
  if (themeName === 'theme-dark') {
    document.querySelector('meta[name="theme-color"]').setAttribute('content', '#ffffff');
    document.querySelector('link[rel="mask-icon"]').setAttribute('color', '#000000');
  } else {
    document.querySelector('meta[name="theme-color"]').setAttribute('content', '#000000');
    document.querySelector('link[rel="mask-icon"]').setAttribute('color', '#ff0059');
  }

  // Смена изображений логотипов
  const logos = document.querySelectorAll('.clients-list__logo a');

  logos.forEach((logo) => {
    if (themeName === 'theme-dark') {
      logo.querySelector('img:last-child').style.display = 'block';
      logo.querySelector('img:first-child').style.display = 'none';
    } else {
      logo.querySelector('img:first-child').style.display = 'block';
      logo.querySelector('img:last-child').style.display = 'none';
    }
  });
}

// Функция переключения между светлой и темной темой
function toggleTheme() {
  let switcher = document.querySelector('.switcher'),
      label = switcher.firstElementChild;

  if (localStorage.getItem('theme') === 'theme-dark') {
    setTheme('theme-light');
    label.textContent = 'На темное';
  } else {
    setTheme('theme-dark');
    label.textContent = 'На светлое';
  }
}

(function () {
  const app = document.getElementById('app');
  app.addEventListener('mousedown', e => e.preventDefault());

  // Немедленно вызываемая функция для установки темы при начальной загрузке
  let switcher = document.querySelector('.switcher'),
      label = switcher.firstElementChild;

  if (localStorage.getItem('theme') === 'theme-dark') {
    setTheme('theme-dark');
    label.textContent = 'На светлое';
  } else {
    setTheme('theme-light');
    label.textContent = 'На темное';
  }

  // Смена темы при клике на переключатель
  switcher.addEventListener('click', function () {
    toggleTheme();
  });

  // Плавно прокручивает к якорю
  const anchors = document.querySelectorAll('.scroll-to');

  anchors.forEach((anchor) => {
    anchor.addEventListener('click', function (event) {
      event.preventDefault();

      let sectionId = anchor.getAttribute('href').substring(1);

      closeMenu();

      document.getElementById(sectionId).scrollIntoView({
        behavior: 'smooth',
        block: 'start'
      });
    });
  });

  // Показывает/скрывает содержимое карточек направлений
  const directionCards = document.querySelectorAll('.directions-list__card');

  directionCards.forEach((card) => {
    card.addEventListener('click', function (event) {
      event.preventDefault();

      if (card.classList.contains('open')) {
        card.classList.remove('open');
      } else {
        directionCards.forEach((card) => {
          card.classList.remove('open');
        });

        card.classList.add('open');
      }
    });
  });

  // Функция открытия popup-окна
  function openPopup(hash) {
    let popup = document.getElementById(hash);

    document.getElementById('popup').style.display = 'block';
    html.style.overflowY = 'hidden';
    html.style.overflowX = 'hidden';

    popup.style.display = 'block';
  }

  // Функция закрытия popup-окна
  function closePopup() {
    let allPopups = document.querySelectorAll('.popup');

    allPopups.forEach((popup) => {
      popup.style.display = 'none';
    });

    history.pushState('', document.title, window.location.pathname);

    html.style.overflowY = 'visible';
    document.getElementById('popup').style.display = 'none';
  }

  // Функция смены popup-окна
  function togglePopup(hash) {
    let allPopups = document.querySelectorAll('.popup');

    allPopups.forEach((popup) => {
      popup.style.display = 'none';
    });

    let popup = document.getElementById(hash);

    popup.style.display = 'block';
  }

  const html = document.querySelector('html'),
        body = document.querySelector('body');

  let hash = decodeURI(window.location.hash).replace('#', ''),
      hashes = ['direct', 'case', 'job']; // Активные окна

  // Открывает окно
  const openLinksPopup = document.querySelectorAll('.open-popup');

  openLinksPopup.forEach((link) => {
    link.addEventListener('click', function (event) {
      event.preventDefault();

      hash = link.getAttribute('href').substring(1);

      let popup = document.getElementById(hash),
          attr = link.getAttribute('data-direction') ?? '';

      if (typeof attr != '') {
        popup.setAttribute('data-direction', attr);
      } else {
        popup.removeAttribute('data-direction')
      }

      if (attr == 'case') {
        togglePopup(hash);
      } else {
        openPopup(hash);
      }

      event.stopPropagation();
    });
  });

  // ..при загрузке страницы
  hashes.forEach((link) => {
    if (hash == link.toString()) openPopup(hash);
  });

  // Закрывает окно
  const closeLinksPopup = document.querySelectorAll('.close-popup');

  closeLinksPopup.forEach((link) => {
    link.addEventListener('click', function (event) {
      event.preventDefault();

      closePopup();
    });
  });

  // ..при нажатии на клавишу Escape
  body.addEventListener('keydown', function (event) {
    if (event.code == 'Escape') closePopup();
  });

  // Функция открытия меню
  function openMenu() {
    let menu = document.getElementById('menu');

    menu.style.display = 'block';
    html.style.overflowY = 'hidden';
    html.style.overflowX = 'hidden';
  }

  // Функция закрытия меню
  function closeMenu() {
    let menu = document.getElementById('menu');

    html.style.overflowY = 'visible';
    menu.style.display = 'none';
  }

  // Открывает меню
  const openLinkMenu = document.querySelector('.open-menu');

  openLinkMenu.addEventListener('click', function (event) {
    event.preventDefault();

    openMenu();

    event.stopPropagation();
  });

  // Закрывает меню
  const closeLinkMenu = document.querySelector('.close-menu');

  closeLinkMenu.addEventListener('click', function (event) {
    event.preventDefault();

    closeMenu();
  });

  // Открывает фильтры
  const addFilters = document.querySelector('.cases-filter__link a');
  const filters = document.querySelector('.cases-filter-list');

  addFilters.addEventListener('click', function (event) {
    event.preventDefault();

    if (filters.classList.contains('open')) {
      filters.classList.remove('open');
      addFilters.firstElementChild.style.transform = 'rotate(0deg)';
    } else {
      filters.classList.add('open');
      addFilters.firstElementChild.style.transform = 'rotate(180deg)';
    }
  });

  // Добавляет/удаляет фильтры
  const allFilters = document.querySelectorAll('.cases-filter-list__item');

  allFilters.forEach((filter) => {
    filter.firstElementChild.addEventListener('click', function (event) {
      event.preventDefault();

      if (filter.classList.contains('active')) {
        filter.classList.remove('active');
      } else {
        filter.classList.add('active');
      }
    });
  });

  // Удаляет все фильтры
  const removeFilters = document.querySelector('.cases-filter-list__reset');

  removeFilters.firstElementChild.addEventListener('click', function (event) {
    event.preventDefault();

    allFilters.forEach((filter) => {
      filter.classList.remove('active');
    });
  });

  // Показывает уведомление об использовании Cookies
  if (typeof getCookie('cookies') === 'undefined') {
    setTimeout(function () {
      document.querySelector('.cookies').style.display = 'flex';
    }, 9000);
  }

  // Скрывает уведомление
  const closeCookies = document.querySelectorAll('.close-cookies');

  closeCookies.forEach((link) => {
    link.addEventListener('click', function (event) {
      event.preventDefault();

      document.querySelector('.cookies').style.display = 'none';

      document.cookie = 'cookies=true;max-age=' + 60*60*24*14 + ';';
    });
  });
})();
