'use strict'

// Swiper
import Swiper, { Navigation } from 'swiper';

// Добавляет слайдер с кейсами
const casesSwiper = new Swiper('.cases-swiper', {
  modules: [Navigation],
  slidesPerView: 1,
  spaceBetween: 16,
  loop: true,
  breakpoints: {
    768: {
      slidesPerView: 2
    },
    1560: {
      slidesPerView: 3,
      spaceBetween: 32
    }
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev'
  }
});

// Добавляет слайдер с фотоотчетом
const caseSwiper = new Swiper('.case-swiper', {
  modules: [Navigation],
  slidesPerView: 'auto',
  spaceBetween: 10,
  breakpoints: {
    1208: {
      spaceBetween: 20
    }
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev'
  }
});

// js
import './js/main';

// css
import './assets/css/main.css';

// scss
import './assets/scss/main.scss';
